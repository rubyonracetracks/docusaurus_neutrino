#!/bin/bash

# AGENDA:
# * Add basic scripts

echo '###############################'
echo 'Chapter 3: Adding Basic Scripts'
echo '###############################'

mv mod-03-all.sh all.sh
mv mod-03-clean.sh clean.sh
mv mod-03-git_check.sh git_check.sh
mv mod-03-server.sh server.sh
mv mod-03-test_app.sh test_app.sh
mv mod-03-test_code.sh test_code.sh

git add .
git commit -m "Added basic scripts"
