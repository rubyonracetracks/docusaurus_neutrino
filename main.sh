#!/bin/bash

echo '*******************************'
echo 'Welcome to Docusaurus Neutrino!'
echo 'This is a script for creating a new Docusaurus App!'
echo ''
echo 'Enter the name of the directory you wish to use for your new app.'
echo 'If you enter a blank, an app name will be chosen for you.'
read APP_NAME

if [ -z "$APP_NAME" ]; then
  DATE=`date +%Y%m%d-%H%M%S-%3N`
  APP_NAME="docusaurus-$DATE"
fi

echo '-----------------'
echo 'Name of your app:'
echo "$APP_NAME"

sh credentials.sh

mkdir -p log
sh exec-main.sh $APP_NAME | tee log/log-$DATE.txt
